package salariati.repository.implementations;

import org.junit.Before;
import org.junit.Test;
import salariati.enumeration.DidacticFunction;
import salariati.model.Employee;

import java.util.List;

import static org.junit.Assert.*;

public class EmployeeImplTest {

    private Employee employeeECPValid01;
    private Employee employeeECPNonValid01;
    private Employee employeeECPNonValid02;

    private Employee employeeBVAValid01;
    private Employee employeeBVANonValid01;
    private Employee employeeBVANonValid02;

    private Employee employee01;

    private Employee oldEmployeeTC01;
    private Employee newEmployeeTC01;

    private Employee oldEmployeeTC02;
    private Employee newEmployeeTC02;

    private Employee oldEmployeeTC03;
    private Employee newEmployeeTC03;

    private Employee oldEmployeeTC04;
    private Employee newEmployeeTC04;

    private Employee oldEmployeeTC05;
    private Employee newEmployeeTC05;

    private List<Employee> ordonatedEmployeeList;

    private EmployeeImpl repositoryImpl;
    private int employeeNr;

    @Before
    public void setUp(){
       repositoryImpl = new EmployeeImpl();
       ordonatedEmployeeList = repositoryImpl.getEmployeeByCriteria();
       employeeNr = repositoryImpl.getEmployeeList().size();

        // ECP Valid
        employeeECPValid01 = new Employee("FirstNameECPVI", "LastNameECPVI", "1234567890123",DidacticFunction.ASISTENT,"1000");

        // ECP Non-Valid - CNP invalid
            // CNP prea scurt
        employeeECPNonValid01 = new Employee("FirstNameECPNI", "LastNameECPNI","1",DidacticFunction.ASISTENT,"1000");
            // CNP prea lung
        employeeECPNonValid02 = new Employee("FirstNameECPNI", "LastNameECPNI","12345678901234567890",DidacticFunction.ASISTENT,"1000");

        //--------------------------------

        // BVA Valid
        employeeBVAValid01 = new Employee("FirstNameBVAVI", "LastNameBVAVI","1234567890123",DidacticFunction.ASISTENT,"1000");

        // BVA Non-Valid - Salariu invalid
            // Salariu este 0
        employeeBVANonValid01 = new Employee("FirstNameBVANIII", "LastNameBVANIII","1234567890123",DidacticFunction.ASISTENT,"0");
            // Salariul este negativ
        employeeBVANonValid02 = new Employee("FirstNameBVANIII", "LastNameBVANIII","1234567890123",DidacticFunction.ASISTENT,"-1");

        //--------------------------------

        employee01 = new Employee("FirstName", "LastName", "1234567890123",DidacticFunction.ASISTENT,"1234");

        // F02_TC01
        oldEmployeeTC01 = new Employee("Popescu","Ion","1234567890123",DidacticFunction.ASISTENT,"1234");
        newEmployeeTC01 = new Employee("Popescu","Vasile","1234567890123",DidacticFunction.ASISTENT,"1234");

        // F02_TC02
        oldEmployeeTC02 = new Employee("Popescu","Ion","1234567890123",DidacticFunction.ASISTENT,"1234");
        newEmployeeTC02 = new Employee("Popescu","Vasile","1234567890123",DidacticFunction.ASISTENT,"1234");

        // F02_TC03
        oldEmployeeTC03 = new Employee("Popescu","Ion","1234567890123",DidacticFunction.ASISTENT,"1234");
        newEmployeeTC03 = new Employee("Dumitrescu","Ion","1234567890123",DidacticFunction.ASISTENT,"1234");

        // F02_TC04
        oldEmployeeTC04 = new Employee("Popescu","Ion","1234567890123",DidacticFunction.ASISTENT,"1234");
        newEmployeeTC04 = new Employee("Dumitrescu","Vasile","1234567890123",DidacticFunction.ASISTENT,"1234");

        // F02_TC05
        oldEmployeeTC05 = new Employee("Popescu","Ion","1234567890123",DidacticFunction.ASISTENT,"1234");
        newEmployeeTC05 = new Employee("Popescu","Ion","1234567890123",DidacticFunction.LECTURER,"1234");
    }

    @Test
    public void modifyEmployee_TC01(){
        assertEquals(0,repositoryImpl.getEmployeeList().size());

        repositoryImpl.modifyEmployee(oldEmployeeTC01,newEmployeeTC01);

        assertEquals(0,repositoryImpl.getEmployeeList().size());
    }

    @Test
    public void modifyEmployee_TC02(){
        repositoryImpl.addEmployee(oldEmployeeTC02);

        assertEquals(1,repositoryImpl.getEmployeeList().size());

        repositoryImpl.modifyEmployee(oldEmployeeTC02,newEmployeeTC02);

        assertEquals(1,repositoryImpl.getEmployeeList().size());
        assertEquals(oldEmployeeTC02.getFunction(),repositoryImpl.getEmployeeList().get(0).getFunction());

        repositoryImpl.deleteEmployee(oldEmployeeTC02);
    }

    @Test
    public void modifyEmployee_TC03(){
        repositoryImpl.addEmployee(employee01);
        repositoryImpl.addEmployee(oldEmployeeTC03);

        assertEquals(2,repositoryImpl.getEmployeeList().size());

        repositoryImpl.modifyEmployee(oldEmployeeTC03,newEmployeeTC03);

        assertEquals(2,repositoryImpl.getEmployeeList().size());
        assertEquals(oldEmployeeTC03.getFunction(),repositoryImpl.getEmployeeList().get(1).getFunction());

        repositoryImpl.deleteEmployee(employee01);
        repositoryImpl.deleteEmployee(oldEmployeeTC03);
    }

    @Test
    public void modifyEmployee_TC04(){
        repositoryImpl.addEmployee(employee01);
        repositoryImpl.addEmployee(oldEmployeeTC04);

        assertEquals(2,repositoryImpl.getEmployeeList().size());

        repositoryImpl.modifyEmployee(oldEmployeeTC04,newEmployeeTC04);

        assertEquals(2,repositoryImpl.getEmployeeList().size());
        assertEquals(oldEmployeeTC04.getFunction(),repositoryImpl.getEmployeeList().get(1).getFunction());

        repositoryImpl.deleteEmployee(employee01);
        repositoryImpl.deleteEmployee(oldEmployeeTC04);
    }

    @Test
    public void modifyEmployee_TC05(){
        repositoryImpl.addEmployee(employee01);
        repositoryImpl.addEmployee(oldEmployeeTC05);

        assertEquals(2,repositoryImpl.getEmployeeList().size());

        repositoryImpl.modifyEmployee(oldEmployeeTC05,newEmployeeTC05);

        assertEquals(2,repositoryImpl.getEmployeeList().size());
        assertEquals(newEmployeeTC05.getFunction(),repositoryImpl.getEmployeeList().get(1).getFunction());

        repositoryImpl.deleteEmployee(employee01);
        repositoryImpl.deleteEmployee(newEmployeeTC05);
    }


    @Test
    public void addEmployee() {
        assertEquals(employeeNr,repositoryImpl.getEmployeeList().size());

        // ECP Valid
        assertTrue(repositoryImpl.addEmployee(employeeECPValid01));
        assertEquals(++employeeNr,repositoryImpl.getEmployeeList().size());

        // CNP prea sucrt
        assertFalse(repositoryImpl.addEmployee(employeeECPNonValid01));
        assertEquals(employeeNr,repositoryImpl.getEmployeeList().size());

        // CNP prea lung
        assertFalse(repositoryImpl.addEmployee(employeeECPNonValid02));
        assertEquals(employeeNr,repositoryImpl.getEmployeeList().size());

        // BVA valid
        assertTrue(repositoryImpl.addEmployee(employeeBVAValid01));
        assertEquals(++employeeNr,repositoryImpl.getEmployeeList().size());

        // Salariu 0
        assertFalse(repositoryImpl.addEmployee(employeeBVANonValid01));
        assertEquals(employeeNr,repositoryImpl.getEmployeeList().size());

        // Salariu negativ
        assertFalse(repositoryImpl.addEmployee(employeeBVANonValid02));
        assertEquals(employeeNr,repositoryImpl.getEmployeeList().size());

        repositoryImpl.deleteEmployee(employeeECPValid01);
        repositoryImpl.deleteEmployee(employeeBVAValid01);
    }

    @Test
    public void getEmployeeByCriteria(){
        Employee e1;
        Employee e2;
        for (int i =1; i<ordonatedEmployeeList.size()-1; i++) {
            e1 = ordonatedEmployeeList.get(i);
            e2 = ordonatedEmployeeList.get(i+1);
            if(Integer.parseInt(e1.getSalary())>Integer.parseInt(e2.getSalary())){
                assertTrue(true);
            }
            else if(Integer.parseInt(e1.getSalary())==Integer.parseInt(e2.getSalary())){
                assertTrue(Integer.parseInt(e1.getCnp().substring(5,7))<Integer.parseInt(e2.getCnp().substring(5,7)));
            }
            else
                fail();
        }
    }
}