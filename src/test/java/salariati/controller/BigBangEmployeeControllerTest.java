package salariati.controller;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import salariati.enumeration.DidacticFunction;
import salariati.model.Employee;
import salariati.repository.implementations.EmployeeImpl;

import java.util.List;

import static org.junit.Assert.*;

public class BigBangEmployeeControllerTest {

    private EmployeeImpl employeeImpl;
    private EmployeeController employeeController;
    private String firstName;
    private String lastName;
    private String CNP;
    private DidacticFunction oldFuntion;
    private DidacticFunction newFunction;
    private String salary;
    private Employee oldEmployee;
    private Employee newEmployee;
    private List<Employee> orderList;
    private Integer listSize;
    private boolean exists;

    @Before
    public void setUp(){
        employeeImpl = new EmployeeImpl();
        employeeController = new EmployeeController(employeeImpl);

        firstName = "Cornea";
        lastName = "Alexandru";
        CNP = "1121220890123";
        oldFuntion = DidacticFunction.ASISTENT;
        newFunction = DidacticFunction.CONFERENTIAR;
        salary = "2000";
        oldEmployee = new Employee(firstName,lastName,CNP,oldFuntion,salary);
        newEmployee = new Employee(firstName,lastName,CNP,newFunction,salary);

        orderList = employeeController.getEmployeesList();

        listSize = orderList.size();

        exists = false;
    }

    // Test A
    @Test
    public void addEmployee() {
        assertSame(orderList.size(),listSize);

        employeeController.addEmployee(oldEmployee);

        orderList = employeeController.getEmployeesList();
        listSize++;
        assertSame(orderList.size(), listSize);

        employeeController.deleteEmployee(oldEmployee);
        orderList = employeeController.getEmployeesList();
        listSize = orderList.size();
    }

    // Test B
    @Test
    public void modifyEmployee() {
        employeeController.addEmployee(oldEmployee);
        employeeController.modifyEmployee(oldEmployee,newEmployee);
        orderList = employeeController.getEmployeesList();

        for (Employee employee:orderList) {
            if( employee.getFirstName().equals(firstName) &&
                employee.getLastName().equals(lastName) &&
                employee.getCnp().equals(CNP) &&
                employee.getFunction().equals(newFunction) &&
                employee.getSalary().equals(salary))

                exists = true;
        }

        assertTrue(exists);

        employeeController.deleteEmployee(newEmployee);
    }

    // Test C
    @Test
    public void getEmployeeByCriteria() {
        Employee e1;
        Employee e2;
        for (int i =1; i<orderList.size()-1; i++) {
            e1 = orderList.get(i);
            e2 = orderList.get(i+1);
            if(Integer.parseInt(e1.getSalary())>Integer.parseInt(e2.getSalary())){
                assertTrue(true);
            }
            else if(Integer.parseInt(e1.getSalary())==Integer.parseInt(e2.getSalary())){
                assertTrue(Integer.parseInt(e1.getCnp().substring(5,7))<Integer.parseInt(e2.getCnp().substring(5,7)));
            }
            else
                fail();
        }
    }

    // Test P -> A -> B -> C
    @Test
    public void integratedPABC() {
        assertSame(orderList.size(),listSize);

        // A:
        employeeController.addEmployee(oldEmployee);
        listSize++;

        // B:
        employeeController.modifyEmployee(oldEmployee,newEmployee);

        // C:
        orderList = employeeController.getEmployeesList();

        // Test A:
        assertSame(orderList.size(),listSize);

        // Test B:

        for (Employee employee:orderList) {
            if( employee.getFirstName().equals(firstName) &&
                    employee.getLastName().equals(lastName) &&
                    employee.getCnp().equals(CNP) &&
                    employee.getFunction().equals(newFunction) &&
                    employee.getSalary().equals(salary))

                exists = true;
        }

        assertTrue(exists);

        // Test C:

        Employee e1;
        Employee e2;
        for (int i =1; i<orderList.size()-1; i++) {
            e1 = orderList.get(i);
            e2 = orderList.get(i+1);
            if(Integer.parseInt(e1.getSalary())>Integer.parseInt(e2.getSalary())){
                assertTrue(true);
            }
            else if(Integer.parseInt(e1.getSalary())==Integer.parseInt(e2.getSalary())){
                assertTrue(Integer.parseInt(e1.getCnp().substring(5,7))<Integer.parseInt(e2.getCnp().substring(5,7)));
            }
            else
                fail();
        }

        // Delete added employee

        employeeController.deleteEmployee(newEmployee);
        orderList = employeeController.getEmployeesList();
        listSize = orderList.size();
    }
}