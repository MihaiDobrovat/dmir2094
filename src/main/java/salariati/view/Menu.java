package salariati.view;

import salariati.controller.EmployeeController;
import salariati.enumeration.DidacticFunction;
import salariati.exception.EmployeeException;
import salariati.model.Employee;
import salariati.repository.implementations.EmployeeImpl;
import salariati.repository.interfaces.EmployeeRepositoryInterface;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Menu {
    private BufferedReader reader =
            new BufferedReader(new InputStreamReader(System.in));

    private EmployeeRepositoryInterface employeesRepository = new EmployeeImpl();
    private EmployeeController employeeController = new EmployeeController(employeesRepository);
    private int  line = employeesRepository.getEmployeeList().size();

    private int showMenu() {

        System.out.println("--Meniu--");
        System.out.println("1.Adauga angajat.");
        System.out.println("2.Schimba functia angajatului.");
        System.out.println("3.Afiseaja angajati.");
        System.out.println("4.Sterge angajat.");
        System.out.println("5.Exit.");
        try {
            return Integer.parseInt(reader.readLine());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public void run() throws IOException, EmployeeException {
        int cmd = showMenu();
        switch (cmd) {
            case 1: {
                String employeeString ="";
                System.out.println("Introduceti numele angajatului: ");
                employeeString += reader.readLine()+";";

                System.out.println("Introduceti prenumele angajatului: ");
                employeeString += reader.readLine()+";";

                System.out.println("Introduceti CNP-ul angajatului: ");
                employeeString += reader.readLine()+";";

                System.out.println("Introduceti functia angajatului: ");
                employeeString += reader.readLine()+";";

                System.out.println("Introduceti salariul angajatului: ");
                employeeString += reader.readLine()+";";

                Employee employee = Employee.getEmployeeFromString(employeeString,++line);
                employeeController.addEmployee(employee);
                System.out.println("Angajatul " + employee.toString() +" a fost adaugat cu succes!");
                break;
            }
            case 2:{
                System.out.println("Introduceti numele angajatului: ");
                String firstName = reader.readLine();

                System.out.println("Introduceti prenumele angajatului: ");
                String lastName = reader.readLine();

                System.out.println("Introduceti noua functie a angajatului: ");
                String function = reader.readLine();

                Employee oldEmployee = new Employee();
                Employee newEmployee = new Employee();

                oldEmployee.setFirstName(firstName);
                oldEmployee.setLastName(lastName);
                switch (function){
                    case "ASISTENT":{
                        newEmployee.setFunction(DidacticFunction.ASISTENT);
                        break;
                    }
                    case "LECTURER":{
                        newEmployee.setFunction(DidacticFunction.LECTURER);
                        break;
                    }
                    case "TEACHER":{
                        newEmployee.setFunction(DidacticFunction.TEACHER);
                        break;
                    }
                    case "CONFERENTIAR":{
                        newEmployee.setFunction(DidacticFunction.CONFERENTIAR);
                        break;
                    }
                }
                employeeController.modifyEmployee(oldEmployee,newEmployee);
                System.out.println("Functia angajatului a fost modificata cu succes!");
                break;
            }
            case 3:{
                for (Employee employee:employeeController.getEmployeesList()) {
                    System.out.println(employee.getFirstName() + " " + employee.getLastName() + " " + employee.getCnp() + " " + employee.getFunction() + " " + employee.getSalary());
                }
                break;
            }
            case 4:{
                System.out.println("Introduceti numele angajatului: ");
                String firstNume = reader.readLine();

                System.out.println("Introduceti prenumele angajatului: ");
                String lastname = reader.readLine();

                Employee employee = new Employee();
                employee.setFirstName(firstNume);
                employee.setLastName(lastname);

                employeeController.deleteEmployee(employee);
                System.out.println("Angajatul a fost sters cu succes!");
                run();
            }
            case 5:{
                break;
            }
            default:{
                System.out.print("Operatia aleasa nu exista!");
                run();
            }

        }
    }
}
