package salariati.repository.implementations;

import java.io.*;
import java.util.*;

import salariati.exception.EmployeeException;

import salariati.model.Employee;

import salariati.repository.interfaces.EmployeeRepositoryInterface;
import salariati.validator.EmployeeValidator;

public class EmployeeImpl implements EmployeeRepositoryInterface {

    private final String employeeDBFile = "C:\\Users\\mihai.dobrovat\\Desktop\\VVSS\\VVSS_Proiect\\src\\main\\java\\salariati\\employeeDB\\employees.txt";
    private EmployeeValidator employeeValidator;
    private List<Employee> employeeArrayList;

    public EmployeeImpl() {
        this.employeeValidator = new EmployeeValidator();
        this.employeeArrayList = getEmployeeByCriteria();
    }

    @Override
    public boolean addEmployee(Employee employee) {
        if( employeeValidator.isValid(employee) ) {
            BufferedWriter bw = null;
            try {
                bw = new BufferedWriter(new FileWriter(employeeDBFile, true));
                bw.write(employee.toString());
                bw.newLine();
                bw.close();
                employeeArrayList.add(employee);
                return true;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    private void writeEmployees(List<Employee> list){
        BufferedWriter bw = null;
        try {
            bw = new BufferedWriter(new FileWriter(employeeDBFile, false));
            for(Employee employee : list){
                bw.write(employee.toString());
                bw.newLine();
            }
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void deleteEmployee(Employee employee) {
        int i=0;
        for(Employee unit : employeeArrayList){
            if((employee.getFirstName().equals(unit.getFirstName())) && (employee.getLastName().equals(unit.getLastName()))){
               employeeArrayList.remove(i);
               writeEmployees(employeeArrayList);
               break;
           }
            i++;
        }

    }

    @Override
    public void modifyEmployee(Employee oldEmployee, Employee newEmployee) {
        for (Employee unit : employeeArrayList) {
            if ((oldEmployee.getFirstName().equals(unit.getFirstName())) && (oldEmployee.getLastName().equals(unit.getLastName()))) {
                unit.setFunction(newEmployee.getFunction());
                writeEmployees(employeeArrayList);
                break;
            }
        }
    }

    @Override
    public List<Employee> getEmployeeList() {
        List<Employee> employeeList = new ArrayList<Employee>();

        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(employeeDBFile));
            String line;
            int counter = 0;
            while ((line = br.readLine()) != null) {
                Employee employee = new Employee();
                try {
                    employee = (Employee) Employee.getEmployeeFromString(line, counter);
                    employeeList.add(employee);
                } catch(EmployeeException ex) {
                    System.err.println("Error while reading: " + ex.toString());
                }
            }
        } catch (FileNotFoundException e) {
            System.err.println("Error while reading: " + e);
        } catch (IOException e) {
            System.err.println("Error while reading: " + e);
        } finally {
            if (br != null)
                try {
                    br.close();
                } catch (IOException e) {
                    System.err.println("Error while closing the file: " + e);
                }
        }

        return employeeList;
    }

    //F03. afisarea salariatilor ordonati descrescator dupa salariu si crescator dupa varsta (CNP).
    @Override
    public List<Employee> getEmployeeByCriteria() {
        List<Employee> employeeList = getEmployeeList();
        Collections.sort(employeeList,new Comparator<Employee>(){
            @Override
            public int compare(Employee es1, Employee es2){
                int booleanCompare = Integer.compare(Integer.parseInt(es2.getSalary()),Integer.parseInt(es1.getSalary()));
                if(booleanCompare != 0 ){
                    return booleanCompare;
                }
                return Integer.compare(Integer.parseInt(es1.getCnp().substring(5,7)),Integer.parseInt(es2.getCnp().substring(5,7)));
            }
        });

        return employeeList;
    }

}
