package salariati.main;

import salariati.exception.EmployeeException;
import salariati.view.Menu;

import java.io.IOException;

//functionalitati
//F01.	 adaugarea unui nou angajat (nume, prenume, CNP, functia didactica, salariul de incadrare);
//F02.	 modificarea functiei didactice (asistent/lector/conferentiar/profesor) a unui angajat;
//F03.	 afisarea salariatilor ordonati descrescator dupa salariu si crescator dupa varsta (CNP).
//F04. stergerea unui salariat.

public class StartApp {

    public static void main(String[] args) {
        Menu menu = new Menu();
        try {
            menu.run();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (EmployeeException e) {
            e.printStackTrace();
        }
    }

}
